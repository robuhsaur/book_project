from venv import create
from django.contrib import admin
from django.urls import path
from books.views import create_magazine, delete_magazine, edit_magazine, show_genre, show_magazine, show_magazines






urlpatterns =[
    path("", show_magazines, name='show_magazine'),
    path('createmag/', create_magazine, name = 'create_magazine'),
    path("<int:pk>/", show_magazine, name='show_magazine'),
    path("<int:pk>/edit", edit_magazine, name='edit_magazine'), 
    path("<int:pk>/delete/", delete_magazine, name='delete_magazine'),
    path("<int:pk>/detail", show_magazine, name = 'show_magazine'),
    path("genre/<int:pk>", show_genre, name = "show_genre")
    
    
]