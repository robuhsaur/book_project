from venv import create
from django.contrib import admin
from django.urls import path
from books.views import create_magazine, create_view, delete_view, edit_magazine, show_books, newcomer_form, show_a_book, show_magazine, update_view 

urlpatterns = [
    path("", show_books, name='show_books'),
    path("newpeople/", newcomer_form, name = 'new_people'),
    path('create/', create_view, name = 'create_book'),
    path("<int:pk>/", show_a_book, name='show_a_book'),
    path("<int:pk>/edit", update_view, name='update_view'), 
    path("<int:pk>/delete/", delete_view, name='delete_view'),
    
]