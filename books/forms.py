from django import forms # imports forms class from django
from books.models import Books, Magazine, Newcomer # import module


class NewcomerForm(forms.ModelForm):
    class Meta:
        model = Newcomer
        fields = [
            "title",
            "description",
        ]
    

class CreateView(forms.ModelForm):
    class Meta:
        model = Books
        exclude = []


class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        exclude = []


