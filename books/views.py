from multiprocessing import context
from urllib import request
from django.shortcuts import get_object_or_404, render, redirect
from books.models import Books, Genre, Magazine
from books.forms import CreateView, NewcomerForm, MagazineForm


# Create your views here.

def show_books(request):
    books = Books.objects.all()
    context = {
        "books": books

    }
    return render (request, "books/list.html", context)


def newcomer_form(request):
    form = NewcomerForm(request.POST) # what does this mean
    context = {}
    if form.is_valid():
        form.save()
    
    context['form'] = form
    
    return render(request, "books/newcomerform.html", context) 


def create_view(request):
    context = {}
    form = CreateView(request.POST or None) # what does this mean
    if form.is_valid():
        form.save()
        return redirect("show_books")
    
    context['form'] = form
    
    return render(request, "books/create.html", context) 

def show_a_book(request, pk):
    context = {
        "book": Books.objects.get(pk=pk) if Books else None,
    }
    return render(request, "books/detail.html", context)


def update_view(request, pk):
    context = {}
    obj = get_object_or_404(Books, pk=pk)
    form = CreateView(request.POST or None, instance = obj)
    if form.is_valid():
        book = form.save()
        return redirect("show_books")
    context["form"] = form
    return render(request, "books/update_view.html", context)



def delete_view(request, pk):
    context = {}
    obj = get_object_or_404(Books, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("show_books")
    return render(request, "books/delete_view.html", context)
    



#=============================================================================

def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines": magazines

    }
    return render (request, "magazines/list_mag.html", context)



def create_magazine(request):
    context = {}
    form = MagazineForm(request.POST or None) # what does this mean
    if form.is_valid():
        magazine = form.save()
        return redirect("show_magazine")
    
    context['form'] = form
    
    return render(request, "create.html", context) 



def show_magazine(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else None,
    }
    return render(request, "magazines/detail.html", context)




def edit_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_books")
    context["form"] = form
    return render(request, "magazines/update.html", context)



def delete_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("show_magazine")
    return render(request, "magazines/delete.html", context)


def show_genre(request, pk):
    specific_genre = Genre.objects.get(pk=pk)
    
    context = {
        "genre": specific_genre,
    } 
    
    return render(request, "magazines/genre.html", context)
