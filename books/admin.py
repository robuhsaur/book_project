from atexit import register
from django.contrib import admin
from books.models import BookReivew, Books, Genre, Magazine, Issue


admin.site.register(Books)  
admin.site.register(Magazine)
admin.site.register(BookReivew)
admin.site.register(Issue)
admin.site.register(Genre)