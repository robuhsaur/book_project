from tkinter import CASCADE
from django.db import models
from django.forms import DateTimeField





class Books(models.Model):
    title = models.CharField(max_length=125)
    authors = models.ManyToManyField("Author", related_name="books", blank=True)   #Many-to-many can have a null relationship, therefore does not require a on_delete
    number_of_pages = models.IntegerField(null=True)
    bookcover = models.URLField(null=True, blank=True)
    isbn = models.IntegerField(null=True)
    in_print = models.BooleanField(null=True)
    publish_date = models.DateField(auto_now=True)
    description = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())


class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name 


class Newcomer(models.Model):
    title = models.CharField(max_length = 125)
    description = models.TextField(null=True)

    def __str__(self):
        return self.title


class CreateView(models.Model):
    title = models.CharField(max_length = 125)
    author = models.TextField(null=True)

    def __str__(self):
        return self.title



class Magazine(models.Model):
    title = models.CharField(max_length=200, unique=True)
    release = models.SmallIntegerField(null=True)
    issued = models.CharField(max_length=200, unique=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.title + " released in " + str(self.release)
    


class BookReivew(models.Model):
    book = models.ForeignKey(Books, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField(null=True)

class Issue(models.Model):
    title = models.CharField(max_length=125, null=True)
    issue = models.IntegerField(null=True)
    publish_date = models.DateField()
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    text = models.TextField(null=True)
    description = models.CharField(max_length=500)
    cover = models.URLField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True)
    

class Genre(models.Model):
    magazines = models.ManyToManyField("Magazine", related_name="genres")
    name = models.CharField(max_length=50, null=True)


    
